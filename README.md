### How to run the app
Open a terminal and:

1. Clone the repo `git clone https://bitbucket.org/anarchos78/suade-task.git`
2. Go into the application directory `cd suade-task`
3. Create a virtual environment for the project, using python 3.6.x and activate it (I prefer ***pyenv***)
4. While in the activated virtual environment run `pip install -r requirements.txt` to install the required packages
5. To run the tests in the projects directory issue the command `python -W ignore:ResourceWarning -m unittest discover`
6. Run the server: `python run.py`

Open a browser and paste in the address bar:
```sh
http://127.0.0.1:5000/customer-documents/v1/report/1
```
You'll see the details of the invoice `#1` in JSON format. Replacing the number at the end of the URL e.g. `2`
will bring up the details of the respective invoice if exists.

You can pass a querystring like:
```sh
http://127.0.0.1:5000/customer-documents/v1/report/1?type=xml
```
It will return a XML document or the respective invoice.
The URL parameter `type` can take the arguments of `xml` or `pdf`. The second will return a PDF invoice.

##### Remarks
- To deploy it, you must use a web server (Apache, Nginx etc) and a WSGI HTTP server (Gunicorn, mod_wsgi etc). 

- The project use flask package `Flask-WeasyPrint`. In order to use it properly, you must install few libraries beforehand.
Read the guide [here](http://weasyprint.readthedocs.io/en/latest/install.html) for platform-specific installation instructions.

- In the `suade-task.config.config.py` file, in the `BaseConfig` class, there is an object named `connection_parameters`.
This object holds the database connection options among others. As you can see, the connection option values are expected 
to be found in the environment.
(this is the best way to set sensitive values in my opinion). For the sake of the exercise, I set default values.
This is not the optimal way! Submitting such info to a repo is not a good idea.

- The unittests are incomplete because the time was up!

**Enjoy!**
