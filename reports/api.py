# -*- coding: utf-8 -*-

"""
.. module:: .api.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os

from flask import (
    Blueprint, jsonify, request, Response, render_template,
    send_from_directory, current_app, make_response, json
)
from flask_weasyprint import HTML, render_pdf

from .models import Report
from .utils import load_reports, create_xml


reports = Blueprint(
    "reports",
    __name__,
    url_prefix="/customer-documents/v1",  # API version
    static_folder="static",
    template_folder="templates",
    static_url_path="/reports/static"
)


@reports.route("/favicon.ico")
def favicon():
    """
    Serves the favicon
    """
    return send_from_directory(
        os.path.join(reports.root_path, "static"),
        "favicon.ico",
        mimetype="image/vnd.microsoft.icon"
    )


@reports.route("/report/<int:report_id>", methods=["GET"])
def get_report(report_id):
    """
    Endpoint responsible for creating/serving xml or pdf reports
    :param report_id: int
    :return: response object
    """

    # Get the report data from the database
    try:
        session = load_reports()
        report = session.query(Report).get(report_id)

        document_type = request.args.get("type")

        if document_type is not None:
            if document_type.lower() == "xml":
                current_app.logger.info(
                    f"Creating the XML report with report_id: {report.id}"
                )

                # For more efficient way to load large files use a generator
                # see http://flask.pocoo.org/docs/0.12/patterns/streaming/
                generator = (line for line in create_xml(report))

                return Response(
                    generator,
                    mimetype="text/xml",
                    headers={
                        "Content-Disposition":
                            f"attachment;filename=report-{report.id}.xml"
                    }
                )

            elif document_type.lower() == "pdf":
                report_data = json.loads(report.data)

                html = render_template(
                    "report.html",
                    organization=report_data["organization"],
                    reported_at=report_data["reported_at"],
                    created_at=report_data["created_at"],
                    inventory=report_data["inventory"]
                )

                current_app.logger.info(
                    f"Creating the PDF report with report_id: {report.id}"
                )

                return render_pdf(
                    HTML(string=html),
                    download_filename=f"report-{report.id}.pdf"
                )
            else:
                current_app.logger.warning(
                    f"Document type \"{document_type}\" is not supported"
                )

        return make_response(jsonify(report.data))
    except AttributeError as err:
        current_app.logger.error(f"An exception occurred, {err}")
        return make_response(jsonify({'error': 'Bad request'}), 400)
