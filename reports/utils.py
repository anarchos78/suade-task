# -*- coding: utf-8 -*-

"""
.. module:: .utils.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import xml.etree.ElementTree as ET

from xml.dom import minidom

from flask import current_app, json

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import mapper, sessionmaker

from .models import Report


def load_reports():
    """
    Returns a SQLAlchemy session to be used for query the `reports` table
    """

    engine = create_engine(
        current_app.config["SQLALCHEMY_DATABASE_URI"],
        echo=False
    )

    metadata = MetaData(engine)
    reports = Table('reports', metadata, autoload=True)

    mapper(Report, reports, non_primary=True)

    Session = sessionmaker(bind=engine)
    session = Session()

    return session


def create_xml(report_data):
    """
    Returns an XML string
    """

    data = json.loads(report_data.data)

    root = ET.Element("report")

    report_id = ET.SubElement(root, "report_id")
    report_id.text = str(report_data.id)

    data_node = ET.SubElement(root, "data")
    organization = ET.SubElement(data_node, "organization")
    reported_at = ET.SubElement(data_node, "reported_at")
    created_at = ET.SubElement(data_node, "created_at")
    inventory = ET.SubElement(data_node, "inventory")

    organization.text = data["organization"]
    reported_at.text = data["reported_at"]
    created_at.text = data["created_at"]

    for item in data["inventory"]:
        item_node = ET.SubElement(inventory, "item")
        item_name = ET.SubElement(item_node, "name")
        item_price = ET.SubElement(item_node, "price")

        item_name.text = item["name"]
        item_price.text = item["price"]

    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ")

    return xmlstr
