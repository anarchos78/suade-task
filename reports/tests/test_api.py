# -*- coding: utf-8 -*-

"""
.. module:: .test_api.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import unittest

from reports.app import create_app


class ApiTests(unittest.TestCase):
    """Tests the reports API"""

    def setUp(self):
        """Setup"""

        self.app = create_app()

        self.app.config['TESTING'] = True
        self.app.config['DEBUG'] = False
        self.app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

        self.client = self.app.test_client()

        self.record_data = {
              "organization": "Test Organization",
              "reported_at": "2018-04-08",
              "created_at": "2018-04-09",
              "inventory": [{
                "name": "item-1",
                "price": "1.00"
              }, {
                "name": "item-2",
                "price": "2.00"
              }, {
                "name": "item-3",
                "price": "3.00"
              }]
        }

    def tearDown(self):
        """Clean up"""

        pass

    def test_favicon(self):
        """Tests favicon endpoint"""

        result = self.client.get('/customer-documents/v1/favicon.ico')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_get_report(self):
        """Tests the get_report endpoint"""

        response = self.client.get(
            '/customer-documents/v1/report/1'
        )

        self.assertIn('organization', str(response.data))
        self.assertIn('created_at', str(response.data))
        self.assertIn('inventory', str(response.data))

        # bad_response = self.client.get(
        #     '/customer-documents/v1/report/10'
        # )
        #
        # self.assertIn('error', bad_response.data.decode())
        # self.assertEqual(bad_response.status_code, 400)


if __name__ == "__main__":
    unittest.main()
