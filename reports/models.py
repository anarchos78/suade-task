# -*- coding: utf-8 -*-

"""
.. module:: .models.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from sqlalchemy import Column, Integer, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Report(Base):
    __tablename__ = 'reports'

    id = Column(Integer, primary_key=True)
    data = Column(Text, nullable=False)
