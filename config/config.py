# -*- coding: utf-8 -*-

"""
.. module:: .config.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os
import logging

from logging.handlers import RotatingFileHandler

from sqlalchemy.engine.url import URL

from reports.api import reports


class BaseConfig(object):
    """
    Base configuration class.
    Here you can define common configuration settings
    that other config classes will inherit from
    """
    DEBUG = False
    TESTING = False

    connection_parameters = {
        "drivername": os.getenv("DRIVER_NAME", "postgres"),
        "username": os.getenv("USERNAME", "interview"),
        "password": os.getenv("PASSWORD", "uo4uu3AeF3"),
        "host": os.getenv("HOST", "candidate.suade.org"),
        "database": os.getenv("DATABASE", "suade"),
        "port": os.getenv("PORT", 5432)
    }

    SQLALCHEMY_DATABASE_URI = URL(**connection_parameters)

    LOGGING_DIR = "logs"
    os.makedirs(LOGGING_DIR, exist_ok=True)

    LOGGING_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    LOGGING_APP_LOCATION = f"{LOGGING_DIR}/service.log"
    LOGGING_DB_LOCATION = f"{LOGGING_DIR}/db.log"
    LOGGING_LEVEL_INFO = logging.INFO
    LOGGING_LEVEL_DEBUG = logging.DEBUG


class DevConfig(BaseConfig):
    """
    Development configuration
    """
    DEBUG = True
    TESTING = True


class TestConfig(BaseConfig):
    """
    Test configuration
    """
    DEBUG = False
    TESTING = True


class ProdConfig(BaseConfig):
    """
    Test Production
    """
    DEBUG = False
    TESTING = False


def configure_app(app):
    """
    App configuration
    """
    app.config.from_object('config.config.ProdConfig')

    app.register_blueprint(reports)

    # Configure app logging
    handler = RotatingFileHandler(
        app.config['LOGGING_APP_LOCATION'], maxBytes=1000000, backupCount=5
    )
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
    app.logger.setLevel(app.config['LOGGING_LEVEL_INFO'])

    # Configure database logging
    db_handler = RotatingFileHandler(
        app.config['LOGGING_DB_LOCATION'], maxBytes=1000000, backupCount=5
    )
    db_handler.setFormatter(formatter)

    db_logger = logging.getLogger('sqlalchemy')
    db_logger.addHandler(db_handler)
    db_logger.setLevel(app.config['LOGGING_LEVEL_DEBUG'])
